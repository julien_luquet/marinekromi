<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clefs secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur 
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C'est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d'installation. Vous n'avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'wordpress');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', '');

/** Adresse de l'hébergement MySQL. */
define('DB_HOST', 'localhost');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8');

/** Type de collation de la base de données. 
  * N'y touchez que si vous savez ce que vous faites. 
  */
define('DB_COLLATE', '');

/**#@+
 * Clefs uniques d'authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant 
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n'importe quel moment, afin d'invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '%3E?lZb=u[HV1J<S<23AMup3d}OL2h@YlGl>EMA3=F_*Ht8=B@D 38i}LoUS_.Hn');
define('SECURE_AUTH_KEY',  '*kd&uGqQZ{RSTh^knGPnx]ynOXGrI},~Y3gKrRuF}lCMcW/</d{&`F B0uZgLg,}');
define('LOGGED_IN_KEY',    'qhDHm,M.?V -CPH3fh8;cMo9b)Qis;4+0?:.6?unlAN5>>Ay]1nbwE ^EO};sgo6');
define('NONCE_KEY',        'VQ&z9iS[utRjmSMa.anhbeyN/~oX; =9Wl>Xm+IG)x6%&8rp4u6qh9~[3}~*-ndX');
define('AUTH_SALT',        '}P#M}F&+Wy;MO@V0eFw.p[=#l{Spv;#vU=U:l?J3+)}B/k+[zL@.VM7U|N09Q;^(');
define('SECURE_AUTH_SALT', '|q 6+A{/}rt,<yU60>DnSpT]=;Z-(2/6z}/C$x@IyMR?$[%Ha{. ???P_*,|GIJK');
define('LOGGED_IN_SALT',   'e|7;$TgxlcwZ]mT[v^>y im-j$Z_K#{2Yh-@zz|ROnmyfQ#_5q:eOCS-v50d7AIy');
define('NONCE_SALT',       'v?[]_U(BT3a/rn%MWm_^+:7(Ls5DhnjA?S:Cw+27*Q`(ZB#*~v?g{L1MfxHyE!,N');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique. 
 * N'utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés!
 */
$table_prefix  = 'wp_';

/**
 * Langue de localisation de WordPress, par défaut en Anglais.
 *
 * Modifiez cette valeur pour localiser WordPress. Un fichier MO correspondant
 * au langage choisi doit être installé dans le dossier wp-content/languages.
 * Par exemple, pour mettre en place une traduction française, mettez le fichier
 * fr_FR.mo dans wp-content/languages, et réglez l'option ci-dessous à "fr_FR".
 */
define('WPLANG', 'fr_FR');

/** 
 * Pour les développeurs : le mode deboguage de WordPress.
 * 
 * En passant la valeur suivante à "true", vous activez l'affichage des
 * notifications d'erreurs pendant votre essais.
 * Il est fortemment recommandé que les développeurs d'extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de 
 * développement.
 */ 
define('WP_DEBUG', false); 

/* C'est tout, ne touchez pas à ce qui suit ! Bon blogging ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');