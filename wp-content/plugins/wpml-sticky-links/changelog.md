**1.3.3**
* **Fix** Handled dependency to icl_js_escape() function
* **Fix** Added support for converting links to Custom Post Types with translated slugs
* **Fix** mysql_* functions doesn't show deprecated notice when PHP >= 5.5
* **Fix** Several fixes to achieve compatibility with WordPress 3.9
* **Fix** Updated links to wpml.org
* **Fix** Handled case where ICL_PLUGIN_PATH constant is not defined (i.e. when plugin is activated before WPML core)

**1.3.2**
* **Fix** Handled dependency from SitePress::get_setting()
* **Fix** Removed dependency to SitePress when instantiating the class
* **Fix** Updated translations
* **Fix** Fixed possible javascript exception in Firefox, when using event.preventDefault();

**1.3.1**
* **Feature** Added WPML capabilities (see online documentation)
* **Fix** SSL support for included CSS and Javascripts now is properly handled
* **Fix** Support for links to custom post type is working now as expected
* **Fix** Links was not changed into sticky when default language was not English. Now it's fixed.