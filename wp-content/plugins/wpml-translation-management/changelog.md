**1.9.4**
* **Improvement** Defining global variables to improve code inspection
* **Fix** Removed notice after "abort translation"
* **Fix** Updated links to wpml.org
* **Fix** Fixed Translation Editor notices in wp_editor()
* **Fix** Handled case where ICL_PLUGIN_PATH constant is not defined (i.e. when plugin is activated before WPML core)

**1.9.3**
* **Fix** Handled dependency from SitePress::get_setting()
* **Fix** Changed vn to vi in locale files
* **Fix** Updated translations
* **Fix** Replace hardcoded references of 'wpml-translation-management' with WPML_TM_FOLDER

**1.9.2**
* **Performances** Reduced the number of calls to *$sitepress->get_current_language()*, *$this->get_active_languages()* and *$this->get_default_language()*, to avoid running the same queries more times than needed
* **Feature** Added WPML capabilities (see online documentation)
* **Fix** Improved SSL support for CSS and JavaScript files